/**
 * Created with JetBrains WebStorm.
 * User: cvrl
 * Date: 4/24/13
 * Time: 8:32 PM
 * File: /core/config/dev.js
 */

'use strict';

angular.module('nd.config', [])
    .config(function ($logProvider) {
        $logProvider.debugEnabled(true);
    })
    .factory('Environment', function () {
        var url = {
            dev: '',
            remote: '',
            test: 'http://gtpd-capstone.police.gatech.edu:8082'
            //test: 'http://gtpd-capstone.police.gatech.edu'
        };

        return {
            name: 'local',
            path: url.test,
            port: 8082,
            config: {'timeout': 20000}
        };
    });