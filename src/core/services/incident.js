/**
 * Created by mcarlson on 4/24/16.
 */

angular.module('nd.services')
    .factory('Incident', function ($http, $q, $log, $filter, Environment, MapFilters) {
        Incident.TWTTR_FLOOR = 500;
        Incident.FB_FLOOR = 300;
        Incident.$$fetching = false;

        Incident.incidents = [];
        Incident.$$incidents = {};

        function Incident(data) {
            data = data || {};
            var self = this;

            self.id = data.id || null;
            self.lat = data.yCoor || null;
            self.lng = data.xCoor || null;
            self.category = 'incident';
            self.callTime = data.callTime || null;
            self.callType = data.callType || 'N/A';
            self.incidentId = data.incidentId || null;
            self.address = data.address || null;
            self.headerInfo = {};
            self.bodyInfo = {};
            self.notes = {};

            self.code = data.code;
            self.color = '#C36D74';   // red

            angular.extend(self.bodyInfo, {
                incidentId: {
                    title: 'ID',
                    data: self.id
                },
                address: {
                    title: 'Address',
                    data: self.address
                }

            });
            angular.extend(self.headerInfo, {
                callType: {
                    title: 'Call Type',
                    data: self.callType
                },
                callTime: {
                    title: 'Call Time',
                    data: self.callTime
                }
            });
            angular.extend(self.notes, {
                notes: {
                    title: 'notes',
                    data: data.notes
                }
            });

        }

        Incident.query = function () {
            var defer = $q.defer(),
                path = Environment.path + '/dispatches/',
                config = _.extend({}, Environment.config);
            Incident.resetIncident();
            $http.get(path, config).then(
                function (response) {
                    if (response.data.length > 0) {
                        for (var i = 0; i < response.data.length; i++) {
                            var newIncident = new Incident(response.data[i]);
                            Incident.$$incidents[newIncident.id] = newIncident;
                            Incident.incidents.push(Incident.$$incidents[newIncident.id]);
                        }
                    }
                    defer.resolve(Incident.incidents);
                },
                function (response) {
                    defer.reject(response);
                }
            );
            return defer.promise;
        };

        Incident.resetIncident= function () {
            Incident.$$incidents = {};
            Incident.incidents = [];
        };

        return Incident;
    });