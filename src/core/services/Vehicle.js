/**
 * Created by mcarlson on 4/24/16.
 */
angular.module('nd.services')
    .factory('Vehicle', function ($http, $q, $log, $filter, Environment, MapFilters) {
        Vehicle.TWTTR_FLOOR = 500;
        Vehicle.FB_FLOOR = 300;
        Vehicle.$$fetching = false;

        Vehicle.vehicles = [];
        Vehicle.$$vehicles = {};

        function Vehicle(data) {
            data = data || {};
            var self = this;

            self.id = data.id || null;
            self.lat = data.yCoor || null;
            self.lng = data.xCoor || null;
            self.date = data.date || null;

            self.officerBadgeNumber = data.officerBadgeNumber || null;
            self.status = data.status || null;
            self.timestamp = data.timestamp || null;

            self.category = 'vehicle';
            self.bodyInfo = {};
            self.headerInfo = {};


            angular.extend(self.headerInfo, {
                statusCode: {
                    title: 'Status',
                    data: self.status
                },
                badgeNumber: {
                    title: 'BadgeNumber',
                    data: self.officerBadgeNumber
                }
            });

            angular.extend(self.bodyInfo, {
                timestamp: {
                    title: 'Timestamp',
                    data: self.timestamp
                }
            });
        }

        Vehicle.query = function () {
            var defer = $q.defer(),
                path = Environment.path + '/vehicles/',
                config = _.extend({}, Environment.config);
            Vehicle.resetVehicles();

            $http.get(path, config).then(
                function (response) {
                    if (response.data.length > 0) {
                        for (var i = 0; i < response.data.length; i++) {
                            var newVehicle = new Vehicle(response.data[i]);
                            Vehicle.$$vehicles[newVehicle.id] = newVehicle;
                            /** this needs to update the vehicles not push **/
                            Vehicle.vehicles.push(Vehicle.$$vehicles[newVehicle.id]);
                        }
                    }
                    defer.resolve(Vehicle.vehicles);
                },
                function (response) {
                    defer.reject(response);
                }
            );
            return defer.promise;
        };

        Vehicle.resetVehicles = function() {
            Vehicle.$$vehicles = {};
            Vehicle.vehicles = [];
        };

        return Vehicle;
    });