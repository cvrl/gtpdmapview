/**
 * Created with WebStorm.
 * User: hunt
 * Date: 9/16/15
 * Time: 2:44 PM
 * File:
 */
angular.module('nd.services')
    .factory('Card',
        function ($http, $q, $log, $filter, Environment,
                  MapFilters, Vehicle, Incident, Camera) {

            Card.TWTTR_FLOOR = 500;
            Card.FB_FLOOR = 300;
            Card.$$fetching = false;

            Card.cards = [];
            Card.$$cards = {};

            function Card(data) {
                var self = this;

                self.data = data || {};
                self.id = data.id || null;
                if (data.category === 'vehicle'
                    || data.category === 'incident'
                    || data.category === 'camera') {
                    self.category = data.category;
                } else {
                    self.category = 'incident';
                }

                self.notes = data.notes || null;
                self.url = data.url || '';
                self.headerInfo = {};
                self.bodyInfo = {};
                self.lat = data.lat;
                self.lng = data.lng;
                self.center = [self.lat, self.lng];

                if (data.category === 'incident') {
                    self.color = '#C36D74';   // red
                } else if (data.category === 'vehicle') {
                    self.color = '#87A5C8';     // blue
                } else if (data.category === 'camera') {
                    self.color = '#A7D2A3';  // green
                }


                angular.extend(self.bodyInfo, data.bodyInfo, data.headerInfo);
            }

            function getDateJSON(dateKey) {
                if (dateKey) {
                    var d = new Date();
                    switch (dateKey) {
                        case 'lastWeek':
                            d.setDate(d.getDate() - 7);
                            d = new Date(d);
                            return d.toJSON();
                        case 'lastMonth':
                            d.setMonth(d.getMonth() - 1);
                            d = new Date(d);
                            return d.toJSON();
                        case 'all':
                            d.setDate(0);
                            return null;
                    }
                } else {
                    $log.debug('No date key provided');
                }
            }

            /***
             * Returns all cards
             * @returns {*}
             */
            Card.query = function () {
                Card.resetCards();

                return $q.all([Vehicle.query(), Incident.query(), Camera.query()])
                    .then(function (response) {

                        if (response && response.length == 3) {
                            for (var i = 0; i < response.length; i++) {
                                var cardArr = response[i];
                                if (cardArr && cardArr.length) {
                                    for (var j = 0; j < cardArr.length; j++) {
                                        if (cardArr[j]) {
                                            var card = new Card(cardArr[j]);
                                            Card.$$cards[card.id] = card;
                                            Card.cards.push(Card.$$cards[card.id]);
                                        }
                                    }
                                }
                            }
                        }

                        return response;
                    });
            };

            /***
             * Use this function to query only the pins inside the bounding box
             * of the map
             *
             *
             * @param bboxParam
             * @param searchParam
             * @returns {*}
             */
            Card.queryBBox = function (bboxParam, searchParam) {
                if (!MapFilters) throw new Error('no MapFilters, cannot request');

                var defer = $q.defer();
                var _dateStart = getDateJSON(MapFilters.activeDateFilter.key),
                    _noiseFloor = MapFilters.activeNoiseFilter.floor;

                if (bboxParam && _dateStart) {
                    var path = Environment.path + '/vehicles',
                        config = _.extend({}, Environment.config);
                    Card.$$fetching = true;

                    $http.get(path, config).then(
                        function (response) {
                            Card.resetCards(); //reset cards
                            if (response.data.length > 0) {
                                for (var i = 0; i < response.data.length; i++) {
                                    if (response.data[i]) {
                                        var newCard = new Card(response.data[i]);
                                        Card.$$cards[newCard.id] = newCard;
                                        Card.cards.push(Card.$$cards[newCard.id]);
                                    }
                                }
                            }
                            Card.$$fetching = false;
                            defer.resolve(Card.cards);
                        },
                        function (response) {
                            Card.$$fetching = false;
                            defer.reject(response);
                        }
                    );
                } else {
                    Card.$$fetching = false;
                    defer.reject('Need bounding box to [queryBBox]');
                }

                return defer.promise;
            };

            // TODO: wiki documentation suggests that its a param but url structure disagrees
            Card.get = function (cardId) {
                var defer = $q.defer();

                if (cardId) {
                    var path = Environment.path + '/cards/' + cardId,
                        config = _.extend({}, Environment.config);

                    $http.get(path, config).then(
                        function (response) {
                            defer.resolve(new Card(response.data));
                        },
                        function (response) {
                            defer.reject(response);
                        }
                    );
                    return defer.promise;
                } else {
                    defer.reject('cardId required');
                }

                return defer.promise;
            };

            Card.resetCards = function () {
                Card.$$cards = {};
                Card.cards = [];
            };

            //TODO proxy web server request

            return Card;
        });
