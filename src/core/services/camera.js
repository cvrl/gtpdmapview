/**
 * Created by mcarlson on 4/24/16.
 */

angular.module('nd.services')
    .factory('Camera', function ($http, $q, $log, $filter, Environment, MapFilters) {
        Camera.TWTTR_FLOOR = 500;
        Camera.FB_FLOOR = 300;
        Camera.$$fetching = false;
        Camera.cameras = [];
        Camera.$$cameras = {};

        function Camera(data) {
            data = data || {};
            var self = this;

            self.id = data.id || null;
            self.cameraName = data.cameraName || '';
            self.category = 'camera';
            self.date = data.date || null;
            self.lat = data.yCoor || null;
            self.lng = data.xCoor || null;
            self.location = data.location || null;
            self.address = data.address || null;
            self.message = data.message || null;

            self.bodyInfo = {};
            self.headerInfo = {};

            angular.extend(self.headerInfo, {
                cameraName: {
                    title: 'Camera Name',
                    data: self.cameraName
                },
                cameraId: {
                    title: 'Camera Id',
                    data: self.id
                }

            });

            angular.extend(self.bodyInfo, {
                location: {
                    title: 'Location',
                    data: self.location
                }
            });
        }

        Camera.query = function () {
            var defer = $q.defer(),
                path = Environment.path + '/cameras/',
                config = _.extend({}, Environment.config);
            Camera.resetCameras();
            $http.get(path, config).then(
                function (response) {
                    if (response.data.length > 0) {
                        for (var i = 0; i < response.data.length; i++) {
                            var newCamera = new Camera(response.data[i]);
                            Camera.$$cameras[newCamera.id] = newCamera;
                            Camera.cameras.push(Camera.$$cameras[newCamera.id]);
                        }
                    }
                    defer.resolve(Camera.cameras);
                },
                function (response) {
                    defer.reject(response);
                }
            );
            return defer.promise;
        };

        Camera.resetCameras = function() {
            Camera.cameras = [];
            Camera.$$cameras = {};
        };

        return Camera;
    });
