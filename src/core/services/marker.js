/**
 * Created with WebStorm.
 * User: cvrl
 * Date: 3/01/16
 * File: marker.js
 */
angular.module('nd.services')
    .service('MarkerCategories', function () {
        this.colors = {
            'incident': '#C36D74',   // red
            'camera': '#A7D2A3',  // green
            'vehicle': '#87A5C8'     // blue
        };

    })
    .factory('Marker', function (MapStyles, MarkerCategories) {
        Marker.sizingBy = "facebook";
        Marker.maxSize = [55, 65]; // ratio of base marker image width:height = 1.0 : 1.17

        function Marker(data) {
            this.id = data.id || null;
            this.CardData = data || {};
            this.category = data.category || 'vehicle'; //world = misc
            this.lat = data.lat || null;
            this.lng = data.lng || null;

            this.pinSize = data.pinSize || {};
            this.pinSize = 1;

            this.buildLeafletMarker();


            Marker.$$markers[this.id] = this;
            Marker.markers = _.toArray(Marker.$$markers);
        }

        Marker.getIconUrl = function (category) {
            if (category) {
                switch (category) {
                    case 'camera':
                        return 'core/assets/markers/pin_green.png';
                    case 'incident':
                        return 'core/assets/markers/pin_red.png';
                    case 'vehicle':
                        return 'core/assets/markers/pin_blue.png';
                }
                if (category === 'active') {
                    return 'core/assets/markers/pin_purple.png'
                }
            }
        };

        /**
         * pinSizeObject: { "twitter": x, "both": y, "facebook": z } ; x,y,z are numbers between 0 and 1 based on
         *                 the largest card returned (each key scaled separately)
         * sizeBy: which key to use
         */
        Marker.prototype.buildIcon = function () {
            var iconSize = [30, 35];
            var shadowSize = [50, 64];
            var iconAnchor = [15, 35];
            var shadowAnchor = [4, 62];
            var popupAnchor = [-1, -22];

            this.icon = {
                iconUrl: Marker.getIconUrl(this.category),
                iconSize: iconSize,
                shadowSize: shadowSize,
                iconAnchor: iconAnchor,
                shadowAnchor: shadowAnchor,
                popupAnchor: popupAnchor
            };

        };

        Marker.prototype.buildLeafletMarker = function () {
            var self = this;
            this.buildIcon();

            Marker.$$leafletMarkers[this.id] = {
                $$id: self.id,
                lat: this.lat,
                lng: this.lng,
                draggable: false,
                icon: this.icon,
                focus: false
            };

            if (this.category == 'camera') {
                Marker.$$leafletMarkers[this.id].message = " ";
            }

            if (Marker.$$prevMarkerId === this.id) {
                Marker.setActiveLeafletMarker(this.id);
            }
        };


        Marker.setActiveLeafletMarker = function (markerId) {
            if (Marker.$$prevMarkerId && Marker.$$prevMarkerId !== markerId) { // reset previous to correct color
                var prevMarker = Marker.$$leafletMarkers[Marker.$$prevMarkerId],
                    prevModel = Marker.$$markers[Marker.$$prevMarkerId];
                if (prevMarker && prevMarker.icon && prevModel) {
                    prevMarker.icon.iconUrl = Marker.getIconUrl(prevModel.category);
                }
            }

            if (markerId && Marker.$$leafletMarkers[markerId]) {
                var marker = Marker.$$leafletMarkers[markerId];
                if (marker && marker.icon) {
                    marker.icon.iconUrl = Marker.getIconUrl('active');
                    Marker.$$prevMarkerId = markerId;
                }
            }
        };

        Marker.updateSizingBy = function (activeSizings) {
            Marker.sizingBy = _.keys(activeSizings).length == 2
                ? 'both'
                : _.keys(activeSizings)[0];

            angular.forEach(Marker.$$leafletMarkers, function (marker, key) {
                if (Marker.$$markers[key]) {
                    Marker.$$markers[key].buildLeafletMarker();
                }
            });
        };

        Marker.markers = [];
        Marker.$$markers = {};
        Marker.$$leafletMarkers = {};

        Marker.reset = function () {
            Marker.$$markers = {};
            Marker.markers = [];
            Marker.$$leafletMarkers = {};
        };

        return Marker;
    });
