/**
 * Created with JetBrains WebStorm.
 * User: apledger
 * Date: 4/24/13
 * Time: 4:27 PM
 * File: /app/mock.js
 */

angular.module('nd.mock', ['nd.app', 'ngMockE2E'])
    // Dummy Calls
    .run(['$httpBackend', '$timeout', '$log', function ($httpBackend, $timeout, $log) {

        var cameras = [
            {
                id: 1,
                cameraName: 'Hemphill x Ferst',
                category: 'camera',
                location: 'Georgia Tech',
                date: '2016-03-15',
                yCoor: 33.778548,
                xCoor: -84.401232,
                streets: 'hemphill/ferst',
                message: 'https://www.youtube.com/embed/Dkm8Hteeh6M'
            },
            {
                id: 3,
                cameraName: '6th x Ferst',
                category: 'camera',
                location: 'Georgia Tech',
                date: '2016-03-15',
                yCoor: 33.777246,
                xCoor: -84.402459,
                streets: '6th/Ferst',
                message: 'https://www.youtube.com/embed/Dkm8Hteeh6M'
            }
        ];

        var vehicles = [
            {
                id: 2,
                category: 'vehicle',
                v_id: '65451331',
                officerBadgeNumber: '1',
                date: '2016-03-15',
                yCoor: 33.781724,
                xCoor: -84.404200,
                status: 'OK',
                timestamp: '14:00'
            },
            {
                id: 4,
                category: 'vehicle',
                v_id: '695465',
                officerBadgeNumber: '2',
                date: '2016-03-15',
                yCoor: 33.775472,
                xCoor: -84.391938,
                status: 'OK',
                timestamp: '14:00'
            }
        ];

        var dispatch = [
            {
                id: 5,
                category: 'incident',
                address: '6 Ferst Drive',
                code: '1',
                date: '2016-03-15',
                callTime:'09:00',
                callType: 'A1',
                yCoor: 33.776982,
                xCoor: -84.392226,
                notes: 'student was looking at his phone while walking and fell down' +
                ' the stairs in the front of the Klaus Advanced Computing Buildind...' +
                ' like really dude?!'
            },
            {
                id: 6,
                address: '782 Techwood Drive',
                callTime:'19:00',
                callType: 'C',
                category: 'incident',
                code: '123',
                date: '2016-02-15',
                yCoor: 33.778564,
                xCoor: -84.395432,
            }
        ];

        //var pins = [
        //    {
        //        pin_id: 1,
        //        category: 'camera',
        //        date: '2016-03-15',
        //        lat: 33.778548,
        //        lng: -84.401232,
        //        address: '180.0.0.0',
        //        streets: 'hemphill/ferst',
        //        message: 'https://www.youtube.com/embed/Dkm8Hteeh6M'
        //    },
        //
        //    {
        //        pin_id: 2,
        //        category: 'vehicle',
        //        v_id: '65451331',
        //        date: '2016-03-15',
        //        lat: 33.781724,
        //        lng: -84.404200
        //    },
        //    {
        //        pin_id: 3,
        //        category: 'camera',
        //        date: '2016-03-15',
        //        lat: 33.777246,
        //        lng: -84.402459,
        //        address: '180.0.0.1',
        //        streets: '6th/Ferst',
        //        message: 'https://www.youtube.com/embed/Dkm8Hteeh6M'
        //    },
        //    {
        //        pin_id: 4,
        //        category: 'vehicle',
        //        v_id: '695465',
        //        date: '2016-03-15',
        //        lat: 33.775472,
        //        lng: -84.391938
        //    },
        //    {
        //        pin_id: 5,
        //        category: 'incident',
        //        code: '1',
        //        date: '2016-03-15',
        //        lat: 33.776982,
        //        lng: -84.392226,
        //        notes: 'student was looking at his phone while walking and fell down' +
        //        ' the stairs in the front of the Klaus Advanced Computing Buildind...' +
        //        ' like really dude?!'
        //    },
        //    {
        //        pin_id: 6,
        //        category: 'incident',
        //        code: '123',
        //        date: '2016-02-15',
        //        lat: 33.778564,
        //        lng: -84.395432
        //    }
        //];

        function randomDate(start, end) {
            return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
        }

        function selectRandom(array) {
            return array[parseInt((Math.random() * array.length), 10)];
        }

        function randomInt(low, high) {
            return parseInt(((Math.random() * (high - low)) + low), 10);
        }

        function randomFloat(low, high, round) {
            return ((Math.random() * (high - low)) + low).toFixed(round || 0);
        }


        //$httpBackend.whenGET('/articles/all').respond(
        //    function (method, url, data, headers) {
        //        return [200, articles, {}];
        //    });

        $httpBackend.whenGET(/pins/).respond(
            function (method, url, data, headers) {
                var param = url.split('?')[1].split('&');
                return [200, pins, {}];
            });

        $httpBackend.whenGET(/vehicles/).respond(
            function (method, url, data, headers) {
                return [200, vehicles, {}];
            });

        $httpBackend.whenGET(/dispatches/).respond(
            function (method, url, data, headers) {
                return [200, dispatch, {}];
            });

        $httpBackend.whenGET('/cameras/').respond(
            function (method, url, data, headers) {
                return [200, cameras, {}];
            });
        $httpBackend.whenGET('/pins/all').respond(
            function (method, url, data, headers) {
                var pins = angular.extend([], vehicles,cameras,dispatch);
                var param = url.split('?')[1].split('&');
                return [200, pins, {}];
            }
        );

        $httpBackend.whenGET('/articles/all').passThrough();
        $httpBackend.whenGET(/views\//).passThrough();
        $httpBackend.whenGET(/\.html/).passThrough();
    }]);

angular.bootstrap(document, ['nd.mock']);
