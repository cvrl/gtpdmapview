/**
 * Created with WebStorm.
 * User: hunt
 * Date: 6/16/15
 * Time: 1:13 PM
 * File:
 */
//Data

angular.module('nd.map')
    .controller('MapCtrl',
        function ($scope, $state, $timeout, $window, $log, $interval, $compile, $q,
                  leafletData, leafletMarkerEvents, MapArchitect, MapStyles, MapFilters,
                  Card, Incident, Camera, Vehicle, Marker, MarkerCategories) {

            $scope.Card = Card;
            $scope.Marker = Marker;
            $scope.MarkerCategories = MarkerCategories;
            $scope.MapFilters = MapFilters;
            angular.extend($scope, MapStyles.defaultConfig);

            $scope.map = {};
            $scope.activeMarker = {};
            $scope.markerModels = {};
            $scope.markers = [];
            $scope.$$markers = {};
            $scope.cardSearch = "";
            $scope.lastActiveMarkerId = null;
            $scope.activeCardCategory = "";


            $scope.cards = {};

            /* These three object literals may be unnecesary */
            $scope.vehicles = {};
            $scope.incidents = {};
            $scope.cameras = {};

            /* noise filters */
            $scope.noiseFilters = _.toArray(MapFilters.noiseFilters);

            /* date filters */
            $scope.dateFilters = _.toArray(MapFilters.dateFilters);

            /* card category filters */
            $scope.categoryFilters = _.toArray(MapFilters.categoryFilters);
            $scope.activeCategoryFilters = MapFilters.categoryFilters;


            /* social metrics filters */
            $scope.listTabs = _.toArray(MapFilters.sidebarTabs);
            $scope.activeListTab = $scope.listTabs[0];
            $scope.setActiveListTab = function (listTab) {
                $scope.activeListTab = listTab;
            };

            /***
             * Removes all markers and creates new markers from the array
             * returned in the response object.
             */
            $scope.requestPins = _.debounce(
                function () {
                    Card.query()
                        .then(function (response) {
                            Marker.reset();
                            if (response && response.length === 3) {
                                for (var i = 0; i < response.length; i++) {
                                    var markerArr = response[i];
                                    if (markerArr && markerArr.length) {
                                        if ($scope.activeCategoryFilters.hasOwnProperty(markerArr[0].category)) {
                                            for (var j = 0; j < markerArr.length; j++) {
                                                new Marker(markerArr[j]);
                                            }
                                        }
                                    }
                                }
                            }

                            $scope.updateModels();
                        });
                }, 0);

            $scope.requestCards = _.debounce(
                function () {
                    Card.query()
                        .then(function (response) {
                            Card.resetCards();
                            if (response && response.length === 3) {
                                for (var i = 0; i < response.length; i++) {
                                    var cardArr = response[i];
                                    if (cardArr && cardArr.length) {
                                        for (var j = 0; j < cardArr.length; j++) {
                                            new Card(cardArr[j]);
                                        }
                                    }
                                }
                            }

                            $scope.updateModels();
                        });
                }
            );

            $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                if (toParams) {
                    $scope.bbox = toParams.in_bbox;
                    $scope.requestPins();
                }
            });

            $scope.queryMap = function () {
                leafletData.getMap('map').then(function (map) {
                    $scope.mapQueried = true;
                    MapArchitect.register(map);
                }, function (err) {
                    $log.debug(err);
                });
            };


            /***
             *
             * Use this to pol for vehicles once that is set up on the backend
             */
                //var POLL = 11000;
                //$interval(function () {
                //    Vehicle.query()
                //        .then(function () {
                //            //$scope.updateModels();
                //        })
                //}, POLL);

            $scope.setActiveMarker = function (card) {
                $log.debug('[activating] marker');
                if (card) {
                    var lastActiveMarkerId = $scope.lastActiveMarkerId;
                    var targetMarker = $scope.markers[card.id];
                    $scope.activeCardCategory = targetMarker.category;
                    if (targetMarker) {
                        Marker.setActiveLeafletMarker(card.id);
                        $scope.centerMarker = {
                            $$id: targetMarker.$$id,
                            lat: targetMarker.lat,
                            lng: targetMarker.lng,
                            category: card.category
                        };
                        debugger;
                        $scope.activeMarker = _.extend({}, $scope.centerMarker);
                        if (lastActiveMarkerId && $scope.markers) {

                            $scope.markers[lastActiveMarkerId].focus = false;
                        }
                        targetMarker.focus = true;
                        $scope.lastActiveMarkerId = card.id;
                    }
                } else {
                    $log.debug('Need source url to nav.');
                }
            };

            $scope.updateMarkers = function (uMarkers) {
                $scope.markers = uMarkers
                    ? _.extend({}, uMarkers)
                    : _.extend({}, Marker.$$leafletMarkers);
            };

            $scope.updateCards = function () {
                $scope.activeCards = _.filter(Card.cards, function (card) {
                    return $scope.activeCategoryFilters.hasOwnProperty(card.category);
                });
                return $scope.activeCards;
            };

            $scope.search = function (predicate) {
                $scope.cardSearch = predicate;
                $scope.requestPins();
            };

            if (!$scope.mapQueried) {
                $scope.queryMap();
            }

            /***
             * This method updates the marker and card models.
             * This update will update the binded data in the templates,
             * and update the view.
             *
             * Angular Template updates: https://docs.angularjs.org/tutorial/step_02
             * @param uMarkers
             */
            $scope.updateModels = function (uMarkers) {
                //update markers with uMarkers or $$leafletMarkers
                $scope.markers = uMarkers
                    ? _.extend({}, uMarkers)
                    : _.extend({}, Marker.$$leafletMarkers);

                /* update map markers */
                var markerModels = {};
                angular.forEach(Marker.$$markers, function (markerModel, key) {
                    if ($scope.activeCategoryFilters.hasOwnProperty(markerModel.category)) {
                        markerModels[key] = markerModel;
                    }
                });

                var markers = {};
                angular.forEach(markerModels, function (markerModel, key) {
                    markers[key] = Marker.$$leafletMarkers[key];
                });

                $scope.updateMarkers(markers);
                $scope.updateCards();
            };

            $scope.$on('leafletDirectiveMarkersClick', function (event, args) {
                var pinIndex = +args;

                if (pinIndex >= 0 && Card.$$cards[pinIndex]) {
                    $scope.setActiveMarker(Card.$$cards[pinIndex]);
                }
            });

            $scope.$on('leafletDirectiveMarker.map.popupopen', function (event, args) {
                var $container = angular.element(args.leafletObject._popup._contentNode);

                if ($container) {
                    var popupTpl = angular.element("<div>\n    <video width=\'300\' height=\'240\' controls><source src=\'Earth.mp4\' type=\'video/mp4\'></video>\n</div>");
                    var popupEl = $compile(popupTpl)($scope);
                    $container.append(popupEl);
                }

            });
        })
    .controller('MapListCtrl', function ($scope, $state, $window, $log, $timeout, leafletData, MapFilters, Card, Marker) {

        $scope.listCollapsed = false;

        $scope.collapseList = function () {
            $scope.listCollapsed = !$scope.listCollapsed;
        };

        $scope.toggleNoiseFilter = function (filter) {
            if (filter && $state.params) {
                $scope.bbox = $state.params.in_bbox;
                MapFilters.setNoiseFilter(filter);
                $scope.requestPins();
                $scope.requestCards();
            }
        };

        $scope.toggleCategoryFilter = function (filter) {
            if (filter) {
                filter.toggle();

                /* add/delete toggled filter from active filters */
                !!filter.toggled
                    ? $scope.activeCategoryFilters[filter.key] = filter
                    : delete $scope.activeCategoryFilters[filter.key];

                /* update map markers */
                var markerModels = {};
                angular.forEach(Marker.$$markers, function (markerModel, key) {
                    if ($scope.activeCategoryFilters.hasOwnProperty(markerModel.category)) {
                        markerModels[key] = markerModel;
                    }
                });
                var markers = {};
                angular.forEach(markerModels, function (markerModel, key) {
                    markers[key] = Marker.$$leafletMarkers[key];
                });

                //if($scope.centerMarker){
                //    if(!$scope.activeCategoryFilters.hasOwnProperty($scope.centerMarker.category)){
                //        $scope.centerMarker = {
                //            $$id: -1
                //
                //        };
                //    }
                //}

                $scope.updateMarkers(markers);
                $scope.updateCards();
            }
        };

    });