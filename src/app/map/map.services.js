/**
 * Created with WebStorm.
 * User: hunt
 * Date: 6/16/15
 * Time: 1:13 PM
 * File:
 */
angular.module('nd.map')
    .service('CenterMarker', function ($log) {
        /* America centered by default */
        this.lat = 33.776159;
        this.lng = -84.393516;
        this.center = [this.lat,this.lng];
        this.zoom = 16;             //leaflet max: 18

        this.get = function () {
            return {
                lat: this.lat,
                lng: this.lng,
                zoom: this.zoom
            };
        };

        this.setLat = function (lat) {
            if (lat) this.lat = lat;
        };

        this.setLng = function (lng) {
            if (lng) this.lng = lng;
        };

        this.setZoom = function (zoom) {
            if (zoom) this.zoom = zoom;
        };

        this.focus = function (marker) {
            if (marker) {
                this.lat = marker.lat;
                this.lng = marker.lng;
                this.zoom = 6;
            } else {
                $log.debug('Need marker');
            }
        }

    })
    .service('MapFilters', function () {
        this.noiseFilters = {
            high: {
                key: 'high',
                icon: 'fa-map-marker fa-3x',
                floor: 1000
            },
            medium: {
                key: 'medium',
                icon: 'fa-map-marker fa-2x',
                floor: 100
            },
            low: {
                key: 'low',
                icon: 'fa-map-marker',
                floor: 1
            }
        };
        this.activeNoiseFilter = this.noiseFilters['medium'];
        this.setNoiseFilter = function (filter) {
            this.activeNoiseFilter = filter;
        };

        this.dateFilters = {
            lastWeek: {
                key: 'lastWeek',
                title: 'Last Week',
                weight: 3
            },
            lastMonth: {
                key: 'lastMonth',
                title: 'Last Month',
                weight: 2
            },
            all: {
                key: 'all',
                title: 'All',
                weight: 1
            }
        };
        this.activeDateFilter = this.dateFilters['lastWeek'];

        this.sidebarTabs = {
            'pins': {
                key: 'pins',
                title: 'Pins',
                icon: 'fa-map-marker',
                btn: 'nd-btn-tab-pins'
            },
            'cameras': {
                key: 'cameras',
                title: 'Camera Playlist',
                icon: 'fa-play',
                btn: 'nd-btn-tab-playlist'
            }
        };

        this.categoryFilters = {
            'vehicle': {
                key: 'vehicle',
                title: 'Vehicle',
                toggled: true,
                icon: 'fa-car',
                btn: 'nd-btn-vehicle',
                toggle: function () {
                    this.toggled = !this.toggled;
                }
            },
            'incident': {
                key: 'incident',
                title: 'incident',
                toggled: true,
                icon: 'fa-warning',
                btn: 'nd-btn-incident',
                toggle: function () {
                    this.toggled = !this.toggled;
                }
            },
            'camera': {
                key: 'camera',
                title: 'Camera',
                toggled: true,
                icon: 'fa-video-camera',
                btn: 'nd-btn-camera',
                toggle: function () {
                    this.toggled = !this.toggled;
                }
            }
        };
    })
    .service('MapStyles', function (CenterMarker) {
        var MAX_SW = new L.LatLng(33.434456, -84.726192);
        var MAX_NE = new L.LatLng(34.087503, -83.990108);
        var MAX_BOUNDS = new L.LatLngBounds(MAX_SW, MAX_NE);     //TODO look in leaflet directive for bounds

        this.icons = {
            default: {
                iconUrl: 'assets/img/markers/leaflet-default-marker.png',
                shadowUrl: 'assets/img/markers/leaflet-default-marker-shadow.png'
            }
        };

        this.centerMarker = CenterMarker.get();

        this.defaultConfig = {
            centerMarker: this.centerMarker,
            defaults: {
                minZoom: 14,
                zoomAnimation: true,
                scrollWheelZoom: true,
                maxbounds: MAX_BOUNDS,
                zoomControlPosition: 'bottomleft'
            }
        }
    })
    .service('MapArchitect', function ($state, $log, leafletData) {
        var self = this;
        this.map = {};

        this.register = function (map) {
            if (map) {
                this.map = map;
                this.boundMapState();
                registerEvents(this.map);
            }
        };

        function _boundMapState(bounds) {
            if (self.map) {
                bounds = bounds || self.map.getBounds();

                var bbox = bounds._southWest.lat + ',' + bounds._southWest.lng +
                    ',' + bounds._northEast.lat + ',' + bounds._northEast.lng;

                $state.go('app.map.list', {in_bbox: bbox});
            }
        }

        var debounceBoundMapState = _.debounce(_boundMapState, 100);

        this.boundMapState = function (bounds) {
            debounceBoundMapState(bounds)
        };


        function registerEvents(map) {
            if (map) {
                map.on('click', function () {
                    $log.debug('[click] event registered on map');
                });

                /* update bounding box on any map move */
                map.on('moveend', function () {
                    $log.debug('[moveend] event registered on map');
                    self.boundMapState();
                });

                /* drag events */
                map.on('dragend', function () {
                    $log.debug('[dragend] event registered on map');
                });

                /* zooming events */
                map.on('zoomstart', function (event) {
                    $log.debug('[zoomstart] event registered on map');
                });

                map.on('zoomend', function (event) {
                    $log.debug('[zoomend] event registered on map');
                });

                map.on('popupopen', function (event) {
                    $log.debug('[popupopen] event registered on map');
                });

                map.on('popupclose', function (event) {
                    $log.debug('[popupclose] event registered on map');

                });
            }
        }
    });