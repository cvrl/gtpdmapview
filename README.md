# GTPD buzzwatch - Senior Capstone Web Application

The mock backend can be found in mock.js in the GTPDMapView directory

# To get project up and running:

You need to have node, npm, bower, sass, ruby installed.  

Then run the following commands:
  
git clone https://github.gatech.edu/dw191/capstone.git
cd capstone/jdBuzzWatch/GTPDMapView 
gem install animation --pre  
npm install  
bower install 
npm install -g grunt-cli   
npm update  
bower update  
grunt develop.mock  

The app should be running on localhost:9000

